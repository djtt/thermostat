#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Encoder tester.
# By Ted van Roon
# v0.1
from encoder_new import Encoder

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

# instantaniate gpio devices
encoderPin1 = 17
encoderPin2 = 27

def onValueChangeRotary(value):
    print('rotaryvalue: ' + str(value))
encoder1 = Encoder(encoderPin1, encoderPin2, onValueChangeRotary)

def main():
    while True:
        pass

if __name__ == "__main__":
    try:
        main()

    except (KeyboardInterrupt, RuntimeError):
        pass