import RPi.GPIO as GPIO

class Encoder:
    def __init__(self, leftPin, rightPin, callback=None):
        self.leftPin = leftPin
        self.rightPin = rightPin
        self.value = 0
        self.state = '00'
        self.direction = None
        self.callback = callback
        GPIO.setup(self.leftPin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.setup(self.rightPin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(self.leftPin, GPIO.BOTH, callback=self.transitionOccurred)  
        GPIO.add_event_detect(self.rightPin, GPIO.BOTH, callback=self.transitionOccurred)

        #define dirNone 0x0
        # Clockwise step.
        self.dirCw =  0x10
        # Anti-clockwise step.
        self.dirCcw = 0x20
        
        self.rotateStart = 0x0

        self.rotateCcwBegin = 0x1
        self.rotateCwBegin = 0x2
        self.rotateStartMiddle = 0x3
        self.rotateCwMiddle = 0x4
        self.rotateCcwBeginMiddle = 0x5

        self.stepTable = [
            # rotateStart (00)
            (self.rotateStartMiddle, self.rotateCwBegin, self.rotateCcwBegin, self.rotateStart),
            # rotateCcwBegin
            (self.rotateStartMiddle | self.dirCcw, self.rotateStart, self.rotateCcwBegin, self.rotateStart),
            # rotateCwBegin
            (self.rotateStartMiddle | self.dirCw, self.rotateCwBegin, self.rotateStart, self.rotateStart),
            # rotateStartMiddle (11)
            (self.rotateStartMiddle, self.rotateCcwBeginMiddle, self.rotateCwMiddle, self.rotateStart),
            # rotateCwMiddle
            (self.rotateStartMiddle, self.rotateStartMiddle, self.rotateCwMiddle, self.rotateStart | self.dirCw),
            # rotateCcwBeginMiddle
            (self.rotateStartMiddle, self.rotateCcwBeginMiddle, self.rotateStartMiddle, self.rotateStart | self.dirCcw),
        ]

        self.state = self.rotateStart

    def transitionOccurred(self, channel):
        p1 = GPIO.input(self.leftPin)
        p2 = GPIO.input(self.rightPin)
        newState = int("{}{}".format(p1, p2))
        print('newstate: ' + str(newState))
        print('currentstate: ' + str(self.state & 0xf))
        indexOfTable = self.stepTable[self.state & 0xf].index(newState)
        print(self.stepTable)
        print('index: ' + str(indexOfTable))
        self.state = self.stepTable[self.state & 0xf][indexOfTable]
        self.callback(self.state & 0x30 )
        print('returnstate' + str(self.state & 0x30))

    def getValue(self):
        return self.value