# Thermostat

**README**

This prorgam wil operate as a thermostat on a raspberry py with a DS18B20 temperature sensor.

**Version:**
0.2

**Usage:**
Import an instantaniate the class, the class takes an minimum and maximum temperature, an output dir and sensor number. This sensornumber makes it possible to instantaniate a number of sensors. On initialize the class will generate a random temperature between the min and max temperature so you get different readings per sensor. I've added a example.py to give you an idea on how to run this prorgam.

**Author**
This prorgam is created by me, Ted van Roon, if you have any questions don't hesitate to contact me via DM.

**Licence**
This prorgam is licensed under GNU GPLv3.

**Language**
This program is written in Python 3.9, not doing anything fancy, so I think this wil work for Python 3.x.

**Todo**
Make it work with Home Assistant.