import datetime
import os
from PIL import ImageFont
from luma.core.render import canvas

from .display.display import get_device



class DisplayControl:
    def __init__(self, _basePath):
        self.getDevice()
        self.device.hide()
        self.tempRead = 0
        self.fontLocation = os.path.join(_basePath, 'fonts/quicksand/Quicksand_Bold.otf')
        self.FontTemp = ImageFont.truetype(self.fontLocation,15)
        self.margin = 0
        self.cx = 0
        self.cy = 0
        self.color = "blue"
        self.todayLastTime = datetime.datetime.now() - datetime.timedelta(seconds=60)
        self.screenState = 0

    def getDevice(self):
        self.device = get_device()

    def displayController(self, _tempRead, _tempToggle, _setTemp):
        now = datetime.datetime.now()
        todayDate = now.strftime("%d %b %y")
        todayTime = now.strftime("%H:%M")
        
        timediff = now - self.todayLastTime
        if timediff.seconds < 60:
            setTemp = _setTemp
            self.device.show()
            with canvas(self.device) as draw:
                self.tempRead = _tempRead
                self.tempToggle = _tempToggle
                if self.screenState < 2:
                    text = "Temp: " + str(self.tempRead) + "C"
                elif self.screenState >= 2:
                    text = "Set: " + str(setTemp) + "C"
                self.drawToScreen(draw, 0, text, self.color)
                text = todayDate
                self.drawToScreen(draw, 1, text, self.color)
                text = todayTime
                self.drawToScreen(draw, 2, text, self.color)
                text = ""
                if self.tempToggle == False:
                    text = "Heating"
                self.drawToScreen(draw, 3, text, self.color)
        elif self.screenState >= 1:
            self.screenState = 0
            self.device.hide()

    def drawToScreen(self, _draw, _row, _text, _color):
        draw = _draw
        row = _row
        text = _text
        color = _color
        draw.text(((self.cx + self.margin), self.cy + (row * 16)), text, fill=color, font=self.FontTemp)

    def setScreenState(self, _value):
        buttonValue = _value
        now = datetime.datetime.now()
        self.todayLastTime = now
        self.screenState += 1
        if self.screenState > 2:
            self.screenState = 1

    def turnOffScreen(self):
        self.device.hide()