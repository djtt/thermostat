import RPi.GPIO as GPIO

class Relay:
    def __init__(self, _pin1, _pin2):
        self.outputPin1 = _pin1
        self.outputPin2 = _pin2
        self.relayState = True
        GPIO.setmode(GPIO.BCM)
        GPIO.setup(self.outputPin1, GPIO.OUT, initial=self.relayState)
        GPIO.setup(self.outputPin2, GPIO.OUT, initial=self.relayState)

    def stateSwitcher(self, state):
        self.relayState = state
        GPIO.output(self.outputPin1, self.relayState)
        GPIO.output(self.outputPin2, self.relayState)