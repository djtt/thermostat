#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Thermostat with display small.
# By Ted van Roon
# v0.2
import configparser
import os
import datetime
import json
import RPi.GPIO as GPIO
import logging
import logging.config

from thermostatControl.thermostatControl import ThermostatControl
from relay.relay import Relay
from pushbutton.pushbutton import Pushbutton
from encoder.encoder import Encoder
from tempSensor.tempSensor import TempSensor
from displayControl.displayControl import DisplayControl
from tempFileExchange.tempFileExchange import TempFileExchange

# get basepath of script for further usage
basePath = os.path.dirname(__file__)

# setup logging
logConfigPath = os.path.join(basePath, 'logging.conf')
logging.config.fileConfig(logConfigPath)
# create logger
logger = logging.getLogger('Thermostat')

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

# convert non string from properties to bool
def str2bool(v):
  return v.lower() in ("yes", "true", "t", "1")

# import and read configfile
configFile = os.path.join(basePath, 'thermostat.conf')
config = configparser.ConfigParser()
config.read(configFile)

# parse variables from config file
debug = str2bool(config['Defaults']['debug'])

# instantaniate tempSensor
baseDir = config['DefaultConfig']['baseDir']
tempSensorName = config['DefaultConfig']['tempSensorBoven']
tempSensor = TempSensor(baseDir, tempSensorName)

# instantaniate thermostatcontrol
thermostatControl = ThermostatControl(config, basePath)

# instantaniate display control
display = DisplayControl(basePath)

# instantaniate gpio devices
relayPin1 = int(config['GpioSetup']['relay1'])
relayPin2 = int(config['GpioSetup']['relay2'])
relayPin3 = int(config['GpioSetup']['relay3'])
relayPin4 = int(config['GpioSetup']['relay4'])
relay1 = Relay(relayPin1, relayPin2)
# relay2 = Relay(relayPin3, relayPin4)
buttonPin1 = int(config['GpioSetup']['button1'])
# button = Pushbutton(buttonPin1)
def onValueChangeButton(value):
    display.setScreenState(value)
    button.setButtonState = 0
button = Pushbutton(buttonPin1, onValueChangeButton)

encoderPin1 = int(config['GpioSetup']['encoder1'])
encoderPin2 = int(config['GpioSetup']['encoder2'])

def onValueChangeRotary(value):
    tempToChange = thermostatControl.getTemperature()
    thermostatControl.setTemperature(tempToChange + value)
encoder1 = Encoder(encoderPin1, encoderPin2, onValueChangeRotary)

# instantaniate tempFileWriter
outFile1 = os.path.join(basePath, config['FileExchange']['outFile1'])
outFile2 = os.path.join(basePath, config['FileExchange']['outFile2'])
inFile = os.path.join(basePath, config['FileExchange']['inFile'])
daysAutomationIn = os.path.join(basePath, config['FileExchange']['daysAutomationIn'])
daysAutomation = os.path.join(basePath, config['FileExchange']['daysAutomation'])
tempFileExchange = TempFileExchange(outFile1, outFile2, inFile, daysAutomation, daysAutomationIn)

def main():
    logger.info('Thermostat started')
    while True:
            tempFileExchange.readNewTempSettingsFromFile(thermostatControl.readDaysAutomationFile, thermostatControl.readTempFromDaysAutomationFile)
            tempRead = tempSensor.readTempC()
            tempFromApi = tempFileExchange.readTempFromFile()
            tempToggle = thermostatControl.thermostatControlProcess(tempRead, tempFromApi)
            tempSet = thermostatControl.setTemp
            tempFileExchange.writeCurrentTempToFile(tempSet, tempRead)
            relay1.stateSwitcher(tempToggle)
            display.displayController(tempRead, tempToggle, tempSet)

if __name__ == "__main__":
    try:
        main()

    except (KeyboardInterrupt, RuntimeError):
        GPIO.cleanup()