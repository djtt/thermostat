import RPi.GPIO as GPIO

class Pushbutton:
    def __init__(self, _pin, callback=None):
        self.inputPin = _pin
        GPIO.setup(self.inputPin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        GPIO.add_event_detect(self.inputPin, GPIO.FALLING, callback=self.transitionOccurred)
        self.buttonState = 0
        self.callback = callback

    def transitionOccurred(self, channel):
        self.buttonState = 1
        self.callback(self.buttonState)

    def getButtonState(self):
        return self.buttonState

    def setButtonState(self, _value):
        self.buttonState = _value