import json
import logging
import os

class TempFileExchange:
    def __init__(self, _outFilePath1, _outFilePath2, _inFilePath, _daysAutomationOut, _daysAutomationIn):
        self.logger = logging.getLogger('Thermostat')
        self.currentSetTemp = 0
        self.currentTemp = 0
        self.outFilePath1 = _outFilePath1
        self.outFilePath2 = _outFilePath2
        self.inFilePath = _inFilePath
        self.daysAutomationOut = _daysAutomationOut
        self.daysAutomationIn = _daysAutomationIn

    def writeCurrentTempToFile(self, _setTemp, _currentTemp):
        self.currentSetTemp = self.writeFile(_setTemp, self.currentSetTemp, self.outFilePath1)
        self.currentTemp = self.writeFile(_currentTemp, self.currentTemp, self.outFilePath2)
    
    def writeFile(self, _temp, _currentTemp, outputFile):
        try:
            if _temp != _currentTemp:
                temp = {'temp': _temp}
                with open(outputFile, 'w') as outfile:
                    json.dump(temp, outfile)
            # self.logger.info('Written: ' + outputFile)
        except:
            self.logger.exception('Not able to wite: ' + outputFile)
        return _temp

    def readTempFromFile(self):
        if os.path.exists(self.inFilePath):
            with open(self.inFilePath) as json_file:
                try:
                    data = json.load(json_file)
                    temp = data['temp']
                    self.logger.info('Read data from: ' + self.inFilePath)
                except ValueError:
                    self.logger.exception('No json data in file')
                    temp = None
            os.remove(self.inFilePath)
            self.logger.info('Deleted file: ' + self.inFilePath)
            return temp

    def readNewTempSettingsFromFile(self, callback=None, callback2=None):
        if os.path.exists(self.daysAutomationIn):
            with open(self.daysAutomationIn) as json_file:
                try:
                    data = json.load(json_file)
                    with open(self.daysAutomationOut, 'w') as outfile:
                        json.dump(data, outfile)
                    self.logger.info('Read data from: ' + self.daysAutomationIn + ' Stored in: ' + self.daysAutomationOut)
                except ValueError:
                    self.logger.exception('No json data in file')
                    data = None
            os.remove(self.daysAutomationIn)
            self.logger.info('Deleted file: ' + self.daysAutomationIn)
            callback(True)
            callback2(True)
            

