import logging
import os
import glob

class TempSensor:
    def __init__(self, _baseDir, _device):
        self.logger = logging.getLogger('Thermostat')
        self.device = _device
        self.baseDir = _baseDir
        # activate sensors temperature sensor
        os.system('modprobe w1-gpio')
        os.system('modprobe w1-therm')
        self.tempRead = 0

    def readTempC(self):
        try:
            lines = self.readTempRaw()
            while lines[0].strip()[-3:] != 'YES':
                #time.sleep(0.2)
                lines = self.readTempRaw()
            equalsPos = lines[1].find('t=')
            if equalsPos != -1:
                tempString = lines[1][equalsPos+2:]
                tempC = int(tempString) / 1000.0 # TEMP_STRING IS THE SENSOR OUTPUT, MAKE SURE IT'S AN INTEGER TO DO THE MATH
                tempC = str(round(tempC, 1)) # ROUND THE RESULT TO 1 PLACE AFTER THE DECIMAL, THEN CONVERT IT TO A STRING
                self.tempRead = tempC
                # self.logger.info('Read temperature from file')
                return self.tempRead
        except:
            self.logger.exception('Could not read temperature of sensor, returned last known temperature.')
            return self.tempRead

    def readTempRaw(self):
        deviceFolder = glob.glob(self.baseDir + self.device)[0]
        deviceFile = deviceFolder + '/w1_slave'
        f = open(deviceFile, 'r')
        lines = f.readlines()
        f.close()
        return lines