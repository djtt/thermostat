import os
import json
import datetime
import logging

class ThermostatControl:
    def __init__(self, _config, _basePath):
        self.logger = logging.getLogger('Thermostat')
        self.now = datetime.datetime.now()
        
        self.config = _config
        self.basePath = _basePath
        self.readDaysAutomationFile(True)

        self.setTemp = 0
        self.readTempFromDaysAutomationFile(True)

        # initialize vars for process
        self.loopLastTime = "Unknown"
        self.counter = 1 
        self.heaterToggle = False
        self.tempRead = 0

    def thermostatControlProcess(self, _tempRead, _tempFromApi):
        now = datetime.datetime.now()
        # todayDate = now.strftime("%d %b %y")
        todayTime = now.strftime("%H:%M")
        loopTime = now.strftime("%H:%M")
        todayStr = now.strftime("%a")
        self.tempReadFromSensor = _tempRead
        self.tempReadFromApi = _tempFromApi

        if self.tempReadFromApi is not None:
            self.setTemp = self.tempReadFromApi
            self.toggleSet()
                
        if loopTime != self.loopLastTime:
            self.loopLastTime = loopTime
            try:
                currentSetTemp = self.setTemp
                self.setTemp = self.propertyTemp(todayStr,todayTime)
                # self.logger.info('setTemp: ' + self.setTemp)
            except:
                # self.logger.info('todayStr: ' + todayStr)
                # self.logger.info('todayTime: ' + todayTime)
                # print(json.dumps(self.daysAutomation, sort_keys=False, indent=4))
                pass
            self.toggleSet()
        return self.heaterToggle

    def propertyTemp(self, _day, _time):
        for each in self.daysAutomation["DaysAutomation"]:
                dayFromConfig = each["day"]
                timeFromConfig = each["time"]
                tempFromConfig = each["temp"]
                # print(dayFromConfig)
                # print(timeFromConfig)
                # print(tempFromConfig)
                if _day == dayFromConfig and _time == timeFromConfig:
                    # print("returning temp")
                    return tempFromConfig
        raise ValueError("No property found.")

    def setTemperature(self, _setTemp):
        self.setTemp = str(_setTemp)
        self.toggleSet()

    def getTemperature(self):
        return int(self.setTemp)

    def toggleSet(self):
        if self.setTemp is not None:
            if float(self.tempReadFromSensor) < float(self.setTemp):
                self.heaterToggle = False
            else:
                self.heaterToggle = True

    def readTempFromDaysAutomationFile(self, start):
        if start == True:
            yesterday = self.now-datetime.timedelta(days=0)
            todayTime = self.now.strftime("%H:%M")
            todayStr = self.now.strftime("%a")
            yesterdayStr = yesterday.strftime("%a")
            # set times for checking intial temperature setting
            dict = {}
            for each in self.daysAutomation["DaysAutomation"]:
                day = each["day"]
                time = each["time"]
                if day in dict:
                    dict[day].append(time)
                else:
                    dict[day] = [time]
            todayList = dict[todayStr]
            yesterdayList = dict[yesterdayStr]

            time0 = max(yesterdayList)
            time1 = min(todayList)
            time2 = max(todayList)
            # set datetime for initial temperature setting
            if todayTime >= time2:
                day = todayStr
                time = time2
            elif todayTime >= time1 and todayTime < time2:
                day = todayStr
                time = time1
            elif todayTime >= time0 and todayTime < time1:
                day = yesterdayStr
                time = time0
            else:
                self.setTemp = 16
            # try to get initial temperature setting from json
            try:
                self.setTemp = self.propertyTemp(day, time)
            except:
                self.setTemp = 17
                self.logger.info('setTemp not from file')
            # self.logger.info('setTemp = ' + str(self.setTemp))

    def readDaysAutomationFile(self, start):
        if start == True:
            daysAutomationPath = self.config['FileExchange']['daysAutomation']
            daysAutomationFile = os.path.join(self.basePath, daysAutomationPath)
            with open(daysAutomationFile) as configFile:
                daysAutomation = json.load(configFile)
            self.logger.info('Read file: ' + daysAutomationFile)
            self.daysAutomation = daysAutomation